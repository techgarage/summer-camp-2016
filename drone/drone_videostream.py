import cv2
import time
import logging

from bebop import Bebop

wnd = None
def video_frame(frame):
    cv2.imshow("Drone", frame)
    cv2.waitKey(1)

def video_start():
    print("Starting video...")


def video_end():
    print("Ending video...")

print("Connecting to drone..")
drone = Bebop()
# Setting video callback functions
drone.video_callbacks(video_start, video_end, video_frame)

# enable and play video for a bit
drone.videoEnable()
print("Connected.")
for i in xrange(500):
    drone.update();
# turn off video
print ("Disabling video")
drone.videoDisable()

cv2.destroyWindow("Drone")


print("Battery:", drone.battery)

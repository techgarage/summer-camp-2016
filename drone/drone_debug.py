from bebop import Bebop
import logging

print("Connecting to drone...")
drone = Bebop(loggingLevel=logging.DEBUG)
print("Connected.")
drone.trim()

drone.wait(5)